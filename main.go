package main

import (
	"jerrykan.com/podgrim/cmd"
)

func main() {
	cmd.Execute()
}
