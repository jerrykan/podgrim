package output

import "fmt"

func PrintlnGreen(args ...interface{}) {
	fmt.Print("\033[32m")
	fmt.Print(args...)
	fmt.Println("\033[0m")
}
