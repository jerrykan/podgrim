package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/output"
	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(stopCmd())
}

func stopCmd() *cobra.Command {
	cmdStop := func(cmd *cobra.Command, args []string) {
		var hosts []string

		if len(args) == 0 {
			var err error

			hosts, err = podman.ListContainers("running")
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			hosts = args
			for _, host := range hosts {
				exists, err := podman.ContainerExists(host)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				if !exists {
					fmt.Println(
						"Container for host '" + host + "' does not exist")
					os.Exit(1)
				}
			}
		}

		for _, host := range hosts {
			running, err := podman.ContainerStatusIs(host, "running")
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if running {
				output.PrintlnGreen("Stopping '" + host + "' ...")
				err := podman.StopContainer(host)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
			}
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts, _ := podman.ListContainers("running")
		return utils.ExcludeFromArray(hosts, args),
			cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "stop [flags] [host] ...",
		Aliases:               []string{"halt"},
		Short:                 "Stop host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdStop,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
