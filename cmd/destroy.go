package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/prompt"
	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(destroyCmd())
}

func destroyCmd() *cobra.Command {
	var force bool

	cmdDestroy := func(cmd *cobra.Command, args []string) {
		var hosts []string

		if len(args) == 0 {
			var err error

			hosts, err = podman.ListContainers()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			hosts = args
			for _, host := range hosts {
				exists, err := podman.ContainerExists(host)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				if !exists {
					fmt.Println(
						"Container for host '" + host + "' does not exist")
					os.Exit(1)
				}
			}
		}

		for _, host := range hosts {
			if force || prompt.Confirm("Destroy '"+host+"' container?") {
				err := podman.DestoryContainer(host)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
			}
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts, _ := podman.ListContainers()
		return utils.ExcludeFromArray(hosts, args),
			cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "destroy [flags] [host] ...",
		Short:                 "Stop and delete host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdDestroy,
		ValidArgsFunction:     validArgs,
	}

	cmd.Flags().BoolVarP(
		&force, "force", "f", false,
		"Delete host container(s) without confirmation")

	return cmd
}
