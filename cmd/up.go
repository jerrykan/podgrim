package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/output"
	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(upCmd())
}

func upCmd() *cobra.Command {
	cmdUp := func(cmd *cobra.Command, args []string) {
		var hosts []string
		if len(args) == 0 {
			hosts = config.ListHosts()
		} else {
			hosts = args
			for _, host := range hosts {
				if !config.HostExists(host) {
					fmt.Println("Host '" + host + "' not found in Podgrimfile")
					os.Exit(1)
				}
			}
		}

		for _, host := range hosts {
			if config.HostImage(host) == "" {
				fmt.Printf(
					"Host image for '%s' not configured in Podgrimfile\n",
					host,
				)
				os.Exit(1)
			}

			exists, err := podman.ContainerImageExists(host)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if !exists {
				fmt.Println("No host image found for host '" + host + "'.")
				fmt.Println("You may need to run `podgrim build " + host + "`")
				os.Exit(1)
			}
		}

		for _, host := range hosts {
			exists, err := podman.ContainerExists(host)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if exists {
				output.PrintlnGreen("Starting '" + host + "' ...")
				err := podman.StartContainer(host)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				continue
			}

			snapshots, err := podman.ListSnapshots(host)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if len(snapshots) > 0 {
				output.PrintlnGreen("Creating '" + host + "' from snapshot ...")
				err := podman.CreateContainerFromSnapshot(
					host, snapshots[len(snapshots)-1])
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				continue
			}

			output.PrintlnGreen("Creating '" + host + "' ...")
			err = podman.CreateContainer(host)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			// HACK: slight delay to allow container to become ready before
			//   running scripts
			time.Sleep(100 * time.Millisecond)

			for _, script := range config.PostScripts(host) {
				output.PrintlnGreen("Running post script: " + script)
				err = podman.ExecContainer(host, script)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
			}
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts := utils.ExcludeFromArray(config.ListHosts(), args)
		return hosts, cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "up [flags] [host] ...",
		Short:                 "Start and provision host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdUp,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
