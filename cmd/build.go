package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/output"
	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(buildCmd())
}

func buildCmd() *cobra.Command {
	tagImage := func(host string, image string) {
		output.PrintlnGreen("  Pulling image '" + image + "' ...")
		err := podman.PullImage(image)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		output.PrintlnGreen("  Tagging container image ...")
		err = podman.TagContainerImage(host, image)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	buildImage := func(host string, image string, buildFile string) {
		if !filepath.IsAbs(buildFile) {
			buildFile = filepath.Join(
				filepath.Dir(config.ConfigFilename()),
				buildFile,
			)
		}

		_, err := os.Stat(buildFile)
		if err != nil {
			fmt.Printf(
				"Buildfile for host '%s' does not exist: %s\n",
				host,
				buildFile,
			)
			os.Exit(1)
		}

		output.PrintlnGreen("  Building ...")
		err = podman.BuildContainerImage(host, image, buildFile)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	runCmd := func(cmd *cobra.Command, args []string) {
		var hosts []string
		if len(args) == 0 {
			hosts = config.ListHosts()
		} else {
			hosts = args
			for _, host := range hosts {
				if !config.HostExists(host) {
					fmt.Println("Host '" + host + "' not found in Podgrimfile")
					os.Exit(1)
				}
			}
		}

		for _, host := range hosts {
			output.PrintlnGreen("Building container image: ", host)
			image := config.HostImage(host)
			if image == "" {
				fmt.Printf("Host '%v' has no 'image' configured\n", host)
				os.Exit(1)
			}

			buildFile := config.HostBuildFile(host)
			if buildFile == "" {
				tagImage(host, image)
				return
			}

			buildImage(host, image, buildFile)
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts := utils.ExcludeFromArray(config.ListHosts(), args)
		return hosts, cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:               "build",
		Short:             "Build host container images",
		Run:               runCmd,
		ValidArgsFunction: validArgs,
	}

	return cmd
}
