package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/snapshot"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(snapshotCmd())
}

func snapshotCmd() *cobra.Command {
	cmdSnapshot := func(cmd *cobra.Command, args []string) {
		var host string

		if len(args) < 1 {
			host = config.DefaultHost()
			if host == "" {
				fmt.Println("Error: No hosts confgured in Podgrimfile")
				os.Exit(1)
			}
		} else {
			host = args[0]
			if !config.HostExists(host) {
				fmt.Println("Host '" + host + "' not found in Podgrimfile")
				os.Exit(1)
			}
		}

		exists, err := podman.ContainerExists(host)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if !exists {
			fmt.Println(
				"Container for host '" + host + "' does not exist")
			os.Exit(1)
		}

		err = podman.CreateSnapshot(host)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	var cmd = &cobra.Command{
		Use:                   "snapshot [flags] [host]",
		Short:                 "Take a snapshot of host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdSnapshot,
	}

	cmd.AddCommand(snapshot.ListCmd())
	cmd.AddCommand(snapshot.PopCmd())

	return cmd
}
