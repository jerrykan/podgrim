package snapshot

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/prompt"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func PopCmd() *cobra.Command {
	var force bool

	cmdPop := func(cmd *cobra.Command, args []string) {
		var host string

		if len(args) < 1 {
			host = config.DefaultHost()
			if host == "" {
				fmt.Println("Error: No hosts confgured in Podgrimfile")
				os.Exit(1)
			}
		} else {
			host = args[0]
			if !config.HostExists(host) {
				fmt.Println("Host '" + host + "' not found in Podgrimfile")
				os.Exit(1)
			}
		}

		snapshots, err := podman.ListSnapshots(host)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if len(snapshots) == 0 {
			fmt.Println("Host container '" + host + "' has no snapshots")
			os.Exit(0)
		}

		snapshot := snapshots[len(snapshots)-1]

		// TODO: check snapshot image is not in use by current container
		if force || prompt.Confirm(
			fmt.Sprintf(
				"Delete host container snaphost '%s:%s' ?",
				host,
				snapshot,
			),
		) {
			err = podman.DeleteSnapshot(host, snapshot, false)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts := []string{}
		if len(args) == 0 {
			hosts = config.ListHosts()
		}
		return hosts, cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "pop [flags] [host]",
		Short:                 "Delete most recent host container snapshot",
		DisableFlagsInUseLine: true,
		Run:                   cmdPop,
		ValidArgsFunction:     validArgs,
	}

	cmd.Flags().BoolVarP(
		&force, "force", "f", false,
		"Delete host container snapshot without confirmation")

	return cmd
}
