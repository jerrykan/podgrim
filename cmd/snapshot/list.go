package snapshot

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func ListCmd() *cobra.Command {
	cmdList := func(cmd *cobra.Command, args []string) {
		var host string

		if len(args) < 1 {
			host = config.DefaultHost()
			if host == "" {
				fmt.Println("Error: No hosts confgured in Podgrimfile")
				os.Exit(1)
			}
		} else {
			host = args[0]
			if !config.HostExists(host) {
				fmt.Println("Host '" + host + "' not found in Podgrimfile")
				os.Exit(1)
			}
		}

		snapshots, err := podman.ListSnapshots(host)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if len(snapshots) == 0 {
			fmt.Println("Host container '" + host + "' has no snapshots")
			os.Exit(0)
		}

		maxSnapshotName := 0
		for _, snapshot := range snapshots {
			if len(snapshot) > maxSnapshotName {
				maxSnapshotName = len(snapshot)
			}
		}

		paddingWidth := fmt.Sprintf("%d", maxSnapshotName)

		var output bytes.Buffer
		fmt.Fprintln(&output, "Host container '"+host+"' snapshots:")
		for _, snapshot := range snapshots {
			created, err := podman.SnapshotCreated(host, snapshot)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			fmt.Fprintf(
				&output,
				"    %-"+paddingWidth+"s    %s\n",
				snapshot,
				created,
			)
		}

		fmt.Println(output.String())
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts := []string{}
		if len(args) == 0 {
			hosts = config.ListHosts()
		}
		return hosts, cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "list [flags] [host]",
		Short:                 "List host container snapshots",
		DisableFlagsInUseLine: true,
		Run:                   cmdList,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
