package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"text/template"

	"github.com/google/uuid"
	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/utils"
)

func init() {
	rootCmd.AddCommand(initCmd())
}

func initCmd() *cobra.Command {
	type ConfigValues struct {
		Name  string
		Id    string
		Image string
	}

	var force bool
	var minimalTemplate bool

	createPodgrimfile := func(name string, image string) {
		current, err := os.Getwd()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		cfg_filename := filepath.Join(current, "Podgrimfile")
		fileInfo, _ := os.Stat(cfg_filename)
		if !force && fileInfo != nil {
			fmt.Fprintln(os.Stderr, "File 'Podgrimfile' already exists")
			os.Exit(1)
		}

		id := uuid.NewString()
		if id == "" {
			fmt.Fprintln(os.Stderr, "Unable to generate unique ID")
			os.Exit(1)
		}

		var templateStr string
		if minimalTemplate {
			templateStr = utils.TemplateMinimal
		} else {
			templateStr = utils.Template
		}

		cfg_template, err := template.New("config").Parse(templateStr)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Unable to parse template", err)
			os.Exit(1)
		}

		cfg_file, err := os.Create(cfg_filename)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Unable to create Podgrimfile:", err)
			os.Exit(1)
		}

		cfg_values := ConfigValues{
			Name:  name,
			Image: image,
			Id:    id[:8] + id[9:11],
		}

		err = cfg_template.Execute(cfg_file, cfg_values)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Unable to create Podgrimfile:", err)
			os.Exit(1)
		}
	}

	cmdRun := func(cmd *cobra.Command, args []string) {
		var name string
		var image string

		if len(args) >= 1 {
			image = args[0]
		} else {
			image = "scratch"
		}

		if len(args) == 2 {
			name = args[1]
		} else {
			name = "default"
		}

		createPodgrimfile(name, image)
	}

	var cmd = &cobra.Command{
		Use:                   "init [flags] [image [name]]",
		Short:                 "Initialise a new Podgrim environment by creating a Podgrimfile",
		Args:                  cobra.MaximumNArgs(2),
		Annotations:           map[string]string{"configInit": "false"},
		DisableFlagsInUseLine: true,
		Run:                   cmdRun,
	}

	cmd.Flags().BoolVarP(
		&force, "force", "f", false, "Overwrite existing Podgrimfile")
	cmd.Flags().BoolVarP(
		&minimalTemplate, "minimal", "m", false, "Create minimal Podgrimfile")

	return cmd
}
