package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

var rootCmd = cmdRoot()

func cmdRoot() *cobra.Command {
	var log string

	cmdPreRun := func(cmd *cobra.Command, args []string) {
		configInit := cmd.Annotations["configInit"]

		if configInit != "false" &&
				cmd.Name() != "__completion" &&
				cmd.Parent().Name() != "completion" {
			config.LoadConfig()
		}

		switch log {
		case "debug":
			podman.Logger["debug"].SetOutput(os.Stdout)
			fallthrough
		case "info":
			podman.Logger["info"].SetOutput(os.Stdout)
		}
	}

	cmd := &cobra.Command{
		Use:              "podgrim",
		Short:            "Podman container life-cycle management",
		PersistentPreRun: cmdPreRun,
	}

	cmd.PersistentFlags().StringVarP(
		&log, "log", "l", "", "Logging level. One of 'info', 'debug'.")
	cmd.RegisterFlagCompletionFunc("log", func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		keys := make([]string, 0, len(podman.Logger))
		for key := range podman.Logger {
			keys = append(keys, key)
		}
		return keys, cobra.ShellCompDirectiveDefault
	})

	return cmd
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
