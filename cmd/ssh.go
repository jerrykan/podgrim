package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(sshCmd())
}

func sshCmd() *cobra.Command {
	generateSshKeys := func() error {
		podgrimDir := config.PodgrimDir()

		_, err := os.Stat(podgrimDir)
		if os.IsNotExist(err) {
			err = os.Mkdir(podgrimDir, 0755)
			if err != nil {
				return fmt.Errorf("Unable to create .podgrim directory: %s", err)
			}
		}

		privateSshKey := config.PrivateSshKey()

		_, err = os.Stat(privateSshKey)
		if !os.IsNotExist(err) {
			_, err = os.Stat(privateSshKey + ".pub")
			if !os.IsNotExist(err) {
				return nil
			}
		}

		sshKeygen := exec.Command(
			"ssh-keygen",
			"-q",
			"-f", privateSshKey,
			"-N", "",
			"-C", "podgrim",
		)
		return sshKeygen.Run()
	}

	insertSshKeys := func(host string) error {
		output, err := podman.ContainerSshKey(host)

		if err == nil {
			content, err := ioutil.ReadFile(config.PublicSshKey())
			if err != nil {
				return fmt.Errorf("Unable to read private SSH key: %s", err)
			}

			// Public keys exists and matches
			if string(content) == output {
				return nil
			}
		}

		return podman.CopySshKeyToContainer(host, config.PublicSshKey())
	}

	sshContainer := func(host string, sshArgs ...string) error {
		ipPort, err := podman.ContainerSshIpPort(host)

		if err != nil {
			return fmt.Errorf("Unable to determine SSH port: %s", err)
		}

		binary, err := exec.LookPath("ssh")
		if err != nil {
			return fmt.Errorf("Exec error: %s", err)
		}

		args := append(
			[]string{
				"ssh",
				"-q",
				"-i", config.PrivateSshKey(),
				"-o", "UserKnownHostsFile=/dev/null",
				"-o", "StrictHostKeyChecking=no",
				"ssh://root@" + ipPort,
			},
			sshArgs...,
		)

		return syscall.Exec(
			binary,
			args,
			os.Environ(),
		)
	}

	cmdSsh := func(cmd *cobra.Command, args []string) {
		var host string
		var sshArgs []string

		if len(args) < 1 || strings.HasPrefix(args[0], "-") {
			host = config.DefaultHost()
			if host == "" {
				fmt.Println("Error: No hosts confgured in Podgrimfile")
				os.Exit(1)
			}
			sshArgs = args
		} else {
			host = args[0]
			sshArgs = args[1:]
		}

		if !config.HostExists(host) {
			fmt.Println("Host '" + host + "' not found in Podgrimfile")
			os.Exit(1)
		}

		running, err := podman.ContainerStatusIs(host, "running")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if !running {
			fmt.Println("Error: Container for host '" + host + "' is not running")
			os.Exit(1)
		}

		err = generateSshKeys()
		if err != nil {
			fmt.Printf("Unable to generate SSH keys: %s\n", err)
			os.Exit(1)
		}

		err = insertSshKeys(host)
		if err != nil {
			fmt.Printf("Unable to generate SSH keys: %s\n", err)
			os.Exit(1)
		}

		err = sshContainer(host, sshArgs...)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		if len(args) >= 1 {
			return []string{}, cobra.ShellCompDirectiveNoFileComp
		}

		hosts, _ := podman.ListContainers("running")
		return utils.ExcludeFromArray(hosts, args),
			cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "ssh [flags] [host] [-- [ssh args]]",
		Short:                 "Make ssh connection to host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdSsh,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
