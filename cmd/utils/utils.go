package utils

func ExcludeFromArray(inputArray []string, exclusions []string) []string {
	var outputArray []string

	for _, item := range inputArray {
		add := true
		for _, exclude := range exclusions {
			if item == exclude {
				add = false
				break
			}
		}
		if add {
			outputArray = append(outputArray, item)
		}
	}

	return outputArray
}
