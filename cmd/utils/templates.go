package utils

const Template = `# Podrimfile

# Unique ID
#   - used to avoid name conflicts with other podgrim project
id = "{{.Id}}"

# Hosts
[hosts]
  # Host definition
  [hosts.{{.Name}}]
    # Container image to use for the host container (required)
    image = "{{.Image}}"

    # Default container
    #   - use this host container as the default for podgrim commands that
    #     require a single host argument.
    #   - if multiple hosts have this set, the first (alphabetically) will be
    #     considered the default.
    default = true

    # Build file to create container image for host
    #  - file location is relative to the directory containing the Podgrimfile.
    #  - 'FROM' directive will be overriden by the 'image' configured above.
#    build_file = "_podgrim/Containerfile"

    # Command arguments to be passed to host containerer on creation
#    command = "/bin/systemd"


    # Custom podman arguments to use when creating a container
#    podman_run_args = [
#        "--privileged",
#    ]

    # Commands/scripts to run inside the container once it has started
#    post_scripts = [
#        "sh -c 'echo \"$(date)\" > /etc/container_created'",
#    ]

    # Shell to invoke in container when running ` + "`" + `podgrim shell` + "`" + `
#    shell = "/bin/zsh"

    # Create a bind mount into container
    #  - "<container dir>" = "<host dir>"
    #  - relative host directories are relative the to directory containing the
    #    Podgrimfile
#    [hosts.{{.Name}}.shared_folders]
#      "/var/www" = "www"
#      "/src" = "~/devel/project"
#      "/srv/files" = "/data/files"

#  [hosts.host2]
#    image = "podgrim/debian10"
#    command = "/usr/sbin/apache2"
`

const TemplateMinimal = `# Podgrimfile
id = "{{.Id}}"

[hosts]
  [hosts.{{.Name}}]
    image = "{{.Image}}"
`
