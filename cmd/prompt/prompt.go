package prompt

import (
	"fmt"
	"strings"
)

func Confirm(prompt string) bool {
	var response string

	for {
		fmt.Printf("%s [y/n]: ", prompt)
		fmt.Scanln(&response)
		response = strings.ToLower(response)

		if response == "yes" || response == "y" {
			return true
		} else if response == "no" || response == "n" {
			return false
		}
	}
}
