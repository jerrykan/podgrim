package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(shellCmd())
}

func shellCmd() *cobra.Command {
	cmdShell := func(cmd *cobra.Command, args []string) {
		var host string

		if len(args) < 1 {
			host = config.DefaultHost()
			if host == "" {
				fmt.Println("Error: No hosts confgured in Podgrimfile")
				os.Exit(1)
			}
		} else {
			host = args[0]
		}

		if !config.HostExists(host) {
			fmt.Println("Host '" + host + "' not found in Podgrimfile")
			os.Exit(1)
		}

		running, err := podman.ContainerStatusIs(host, "running")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if !running {
			fmt.Println("Error: Container for host '" + host + "' is not running")
			os.Exit(1)
		}

		err = podman.ShellContainer(host)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		if len(args) >= 1 {
			return []string{}, cobra.ShellCompDirectiveNoFileComp
		}

		hosts, _ := podman.ListContainers("running")
		return utils.ExcludeFromArray(hosts, args),
			cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "shell [flags] [host]",
//		Aliases:               []string{"exec"},
		Short:                 "Open shell prompt in host container",
		DisableFlagsInUseLine: true,
		Run:                   cmdShell,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
