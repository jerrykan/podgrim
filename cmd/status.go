package cmd

import (
	"bytes"
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"jerrykan.com/podgrim/cmd/utils"
	"jerrykan.com/podgrim/config"
	"jerrykan.com/podgrim/podman"
)

func init() {
	rootCmd.AddCommand(statusCmd())
}

func statusCmd() *cobra.Command {
	cmdStatus := func(cmd *cobra.Command, args []string) {
		hosts := args
		if len(hosts) == 0 {
			hosts = config.ListHosts()
		}

		maxHostName := 0
		for _, host := range hosts {
			if !config.HostExists(host) {
				fmt.Println("Host '" + host + "' not found in Podgrimfile")
				os.Exit(1)
			}
			if len(host) > maxHostName {
				maxHostName = len(host)
			}
		}

		paddingWidth := fmt.Sprintf("%d", maxHostName)

		var output bytes.Buffer

		fmt.Fprintln(&output, "Current container states:")
		for _, host := range hosts {
			status, err := podman.ContainerStatus(host)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			fmt.Fprintf(&output, "    %-"+paddingWidth+"s    %s\n", host, status)
		}
		fmt.Println(output.String())
	}

	validArgs := func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		hosts := utils.ExcludeFromArray(config.ListHosts(), args)
		return hosts, cobra.ShellCompDirectiveNoFileComp
	}

	var cmd = &cobra.Command{
		Use:                   "status [flags] [host] ...",
		Aliases:               []string{"halt"},
		Short:                 "Output status of host containers",
		DisableFlagsInUseLine: true,
		Run:                   cmdStatus,
		ValidArgsFunction:     validArgs,
	}

	return cmd
}
