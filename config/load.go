package config

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

func LoadConfig() {
	current, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	var config string

	for {
		config = filepath.Join(current, "Podgrimfile")
		fileInfo, _ := os.Stat(config)

		if fileInfo != nil {
			if fileInfo.Mode().IsRegular() {
				break
			}
		}

		current = filepath.Dir(current)

		if current == "/" {
			fmt.Println("No Podgrimfile found")
			os.Exit(1)
		}
	}

	viper.SetConfigFile(config)
	viper.SetConfigType("toml")
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	id := viper.GetString("id")
	if id == "" {
		fmt.Println("No 'id' configure in Podgrimfile")
		os.Exit(1)
	}
}
