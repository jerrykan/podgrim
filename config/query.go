package config

import (
	"path/filepath"
	"sort"

	"github.com/spf13/viper"
)

func ConfigFilename() string {
	return viper.ConfigFileUsed()
}

func Domain() string {
	return viper.GetString("domain")
}

func DefaultHost() string {
	hosts := ListHosts()
	if len(hosts) == 0 {
		return ""
	}

	for _, host := range hosts {
		if viper.GetBool("hosts." + host + ".default") {
			return host
		}
	}

	return hosts[0]
}

func ID() string {
	return viper.GetString("id")
}

func HostExists(host string) bool {
	return viper.IsSet("hosts." + host)
}

func ListHosts() []string {
	var hosts []string
	for host := range viper.GetStringMap("hosts") {
		hosts = append(hosts, host)
	}

	sort.Strings(hosts)
	return hosts
}

func HostBuildFile(host string) string {
	return viper.GetString("hosts." + host + ".build_file")
}

func HostContainerCommand(host string) string {
	return viper.GetString("hosts." + host + ".command")
}

func HostImage(host string) string {
	return viper.GetString("hosts." + host + ".image")
}

func Hostname(host string) string {
	return viper.GetString("hosts." + host + ".hostname")
}

func PodmanRunArgs(host string) []string {
	return viper.GetStringSlice("hosts." + host + ".podman_run_args")
}

func PodgrimDir() string {
	return filepath.Join(ProjectDir(), ".podgrim")
}

func PrivateSshKey() string {
	return filepath.Join(PodgrimDir(), "podgrim_id_rsa")
}

func PublicSshKey() string {
	return PrivateSshKey() + ".pub"
}

func ProjectDir() string {
	return filepath.Dir(viper.ConfigFileUsed())
}

func PostScripts(host string) []string {
	return viper.GetStringSlice("hosts." + host + ".post_scripts")
}

func SharedFolders(host string) map[string]string {
	projectDir := ProjectDir()
	cfgFolders := viper.GetStringMapString("hosts." + host + ".shared_folders")

	folders := make(map[string]string)
	for containerDir, hostDir := range cfgFolders {
		if filepath.IsAbs(hostDir) {
			folders[containerDir] = hostDir
		} else {
			folders[containerDir] = filepath.Join(projectDir, hostDir)
		}
	}

	return folders
}

func Shell(host string) string {
	shell := viper.GetString("hosts." + host + ".shell")

	if shell == "" {
		shell = "/bin/bash"
	}

	return shell
}
