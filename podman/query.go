package podman

import (
	"fmt"
	"regexp"
	"sort"
	"strings"
)

func ContainerImageExists(host string) (bool, error) {
	imageName := containerImageName(host)

	output, err := PodmanOutput(
		"images",
		"--filter=reference=localhost/"+imageName+":latest",
		"--format='{{.Repository}}'",
	)

	if err != nil {
		return false, fmt.Errorf("Unable to query images: %s", err)
	}

	return len(output) != 0, nil
}

func ContainerExists(host string) (bool, error) {
	name := containerName(host)

	output, err := PodmanOutput(
		"ps",
		"--all",
		"--filter=name=^"+name+"$",
		"--format={{.Names}}",
	)

	if err != nil {
		return false, fmt.Errorf("Unable to query containers: %s", err)
	}

	return len(output) != 0, nil
}

func ContainerSshKey(host string) (string, error) {
	name := containerName(host)

	return PodmanOutput(
		"exec",
		"--user", "root",
		name,
		"cat", sshAuthorizedKeysFile,
	)
}

func ContainerSshIpPort(host string) (string, error) {
	name := containerName(host)

	output, err := PodmanOutput("port", name, "22")
	if err != nil {
		return "", fmt.Errorf("Unable to query container ssh port: %s", err)
	}

	return strings.TrimSpace(string(output)), nil
}

func ContainerStatus(host string) (string, error) {
	name := containerName(host)

	output, err := PodmanOutput(
		"ps",
		"--all",
		"--filter=name=^"+name+"$",
		"--format={{.Status}}",
	)

	if err != nil {
		return "", fmt.Errorf("Unable to query containers: %s", err)
	}

	var status string
	if len(output) == 0 {
		status = "Not created"
	} else {
		status = strings.TrimSpace(string(output))
	}

	return status, nil
}

func ContainerStatusIs(host string, status string) (bool, error) {
	name := containerName(host)

	output, err := PodmanOutput(
		"ps",
		"--all",
		"--filter=name=^"+name+"$",
		"--filter=status="+status,
		"--format={{.Status}}",
	)

	if err != nil {
		return false, fmt.Errorf("Unable to query containers: %s", err)
	}

	return len(output) != 0, nil
}

func ListContainers(statuses ...string) ([]string, error) {
	containersPrefix := containersPrefix()

	args := []string{
		"ps",
		"--all",
		"--filter=name=^" + containersPrefix + ".+$",
		"--format={{.Names}}",
	}
	for _, status := range statuses {
		args = append(args, "--filter=status="+status)
	}

	output, err := PodmanOutput(args...)
	if err != nil {
		return []string{}, fmt.Errorf("Unable to query containers: %s", err)
	}

	var hosts []string
	containers := strings.Split(strings.TrimSpace(output), "\n")
	for _, container := range containers {
		hosts = append(hosts, strings.TrimPrefix(container, containersPrefix))
	}

	return hosts, nil
}

func ListSnapshots(host string) ([]string, error) {
	name := containerImageName(host)

	output, err := PodmanOutput(
		"images",
		"--filter=reference=localhost/"+name,
		"--format={{.Repository}}:{{.Tag}}",
	)
	if err != nil {
		return []string{}, err
	}

	var snapshots []string
	for _, snapshot := range strings.Split(strings.TrimSpace(output), "\n") {
		matched, _ := regexp.MatchString(
			`^localhost/`+name+`:snapshot-\d\d`,
			snapshot,
		)
		if matched {
			snapshots = append(snapshots, strings.Split(snapshot, ":")[1])
		}
	}

	sort.Strings(snapshots)
	return snapshots, nil
}
