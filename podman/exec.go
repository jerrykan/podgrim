package podman

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"
)

func PodmanOutput(args ...string) (string, error) {
	logDebug(args...)

	podman := exec.Command("podman", args...)
	output, err := podman.Output()

	if err != nil {
		exitError, ok := err.(*exec.ExitError)

		if ok {
			errString := strings.TrimSpace(string(exitError.Stderr))
			return "", fmt.Errorf("Podman %s", errString)
		}

		return "", fmt.Errorf("Podman %s", err)
	}

	return string(output), nil
}

func PodmanRun(args ...string) error {
	logInfo(args...)

	stderr := new(bytes.Buffer)

	podman := exec.Command("podman", args...)
	podman.Stdout = Logger["debug"].Writer()
	podman.Stderr = stderr
	err := podman.Run()

	if err != nil {
		errString := strings.TrimSpace(stderr.String())
		return fmt.Errorf("Podman %s", errString)
	}

	return nil
}

func PodmanShell(args ...string) error {
	logInfo(args...)

	binary, err := exec.LookPath("podman")
	if err != nil {
		return fmt.Errorf("Exec error: %s", err)
	}

	return syscall.Exec(
		binary,
		append([]string{"podman"}, args...),
		os.Environ(),
	)
}
