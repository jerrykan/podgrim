package podman

import (
	"fmt"
)

func BuildContainerImage(host string, image string, buildFile string) error {
	imageName := containerImageName(host)

	err := PodmanRun(
		"build",
		"--file="+buildFile,
		"--from="+image,
		"--tag="+imageName,
	)
	if err != nil {
		return fmt.Errorf("Unable to build container image: %v", err)
	}

	return nil
}

func PullImage(image string) error {
	err := PodmanRun("pull", image)

	if err != nil {
		return fmt.Errorf("Unable to pull image: %v", err)
	}

	return nil
}

func TagContainerImage(host string, image string) error {
	imageName := containerImageName(host)

	err := PodmanRun("tag", image, imageName)
	if err != nil {
		return fmt.Errorf("Unable to tag container image: %v", err)
	}

	return nil
}
