package podman

import (
	"fmt"
	"strings"
)

func CreateContainerFromSnapshot(host string, snapshot string) error {
	return createContainerFromImage(
		host, containerImageName(host)+":"+snapshot)
}

func CreateSnapshot(host string) error {
	snapshots, err := ListSnapshots(host)
	if err != nil {
		return err
	}

	image := containerImageName(host)
	container := containerName(host)
	snapshotNumber := len(snapshots) + 1

	return PodmanRun(
		"commit",
		container,
		fmt.Sprintf("%s:snapshot-%02d", image, snapshotNumber),
	)
}

func DeleteSnapshot(host string, snapshot string, force bool) error {
	snapshotImage := containerImageName(host) + ":" + snapshot

	if force {
		return PodmanRun("rmi", "--force", snapshotImage)
	} else {
		return PodmanRun("rmi", snapshotImage)
	}
}

func SnapshotCreated(host string, snapshot string) (string, error) {
	image := containerImageName(host)

	output, err := PodmanOutput(
		"images",
		"--format={{.CreatedSince}}",
		image+":"+snapshot,
	)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(output)), nil
}
