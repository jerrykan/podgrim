package podman

import (
	"io/ioutil"
	"log"
)

var Logger = map[string]*log.Logger{
	"info":  log.New(ioutil.Discard, "INFO[podman]: ", 0),
	"debug": log.New(ioutil.Discard, "DEBUG[podman]: ", 0),
}

func logDebug(args ...string) {
	s := make([]interface{}, len(args)+1)
	s[0] = "podman"
	for i, v := range args {
		s[i+1] = v
	}
	Logger["debug"].Println(s...)
}

func logInfo(args ...string) {
	s := make([]interface{}, len(args)+1)
	s[0] = "podman"
	for i, v := range args {
		s[i+1] = v
	}
	Logger["info"].Println(s...)
}
