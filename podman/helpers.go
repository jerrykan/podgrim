package podman

import (
	"fmt"

	"jerrykan.com/podgrim/config"
)

func containerImageName(host string) string {
	id := config.ID()
	return fmt.Sprintf("%s-%s-%s-image", containerPrefix, id, host)
}

func containerName(host string) string {
	id := config.ID()
	return fmt.Sprintf("%s-%s-%s", containerPrefix, id, host)
}

func containerHostname(host string) string {
	hostname := config.Hostname(host)
	if hostname != "" {
		return hostname
	}

	domain := config.Domain()
	if domain != "" {
		return fmt.Sprintf("%s.%s", host, domain)
	}

	return host
}

func containerOptions(host string) []string {
	var opts []string

	// Shared Folders
	for containerDir, hostDir := range config.SharedFolders(host) {
		opts = append(opts, "--volume="+hostDir+":"+containerDir)
	}

	// Custom podman arguments
	run_args := config.PodmanRunArgs(host)
	opts = append(opts, run_args...)

	return opts
}

func containersPrefix() string {
	id := config.ID()
	return fmt.Sprintf("%s-%s-", containerPrefix, id)
}
