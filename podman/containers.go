package podman

import (
	"fmt"
	"path"

	"github.com/kballard/go-shellquote"

	"jerrykan.com/podgrim/config"
)

func createContainerFromImage(host string, image string) error {
	name := containerName(host)
	hostname := containerHostname(host)
	opts := containerOptions(host)

	args := []string{
		"run",
		"--detach",
		"--name=" + name,
		"--hostname=" + hostname,
		"--publish=127.0.0.1::22",
		"--volume=" + config.ProjectDir() + ":/podgrim",
	}
	args = append(args, opts...)
	args = append(args, image)

	cmd := config.HostContainerCommand(host)
	cmd_args, err := shellquote.Split(cmd)
	if err != nil {
		return fmt.Errorf("Invalid container command (%v): %v", err, cmd)
	}
	if len(cmd_args) > 0 {
		args = append(args, cmd_args...)
	}

	err = PodmanRun(args...)
	if err != nil {
		return fmt.Errorf("Unable to create podman container: %v", err)
	}

	return nil
}

func CreateContainer(host string) error {
	return createContainerFromImage(host, containerImageName(host))
}

func DestoryContainer(host string) error {
	name := containerName(host)

	err := PodmanRun("rm", "--force", name)
	if err != nil {
		return fmt.Errorf("Unable to destroy podman container: %v", err)
	}

	return nil
}

func ExecContainer(host string, script string) error {
	name := containerName(host)

	args := []string{
		"exec",
		name,
	}
	cmd_args, err := shellquote.Split(script)
	if err != nil {
		return fmt.Errorf("Invalid shell command (%v): %v", err, script)
	}
	args = append(args, cmd_args...)

	err = PodmanRun(args...)
	if err != nil {
		return fmt.Errorf("Unable to execute in podman container: %v", err)
	}

	return nil
}

func CopySshKeyToContainer(host string, privateSshKeyFile string) error {
	name := containerName(host)

	err := PodmanRun(
		"exec",
		"--user", "root",
		name,
		"mkdir", "-p", path.Dir(sshAuthorizedKeysFile),
	)
	if err != nil {
		return fmt.Errorf(
			"Unable to create directory for authorized_keys file in podman container: %v",
			err)
	}

	err = PodmanRun(
		"cp",
		privateSshKeyFile,
		name+":"+sshAuthorizedKeysFile,
	)
	if err != nil {
		return fmt.Errorf("Unable to copy file to podman container: %v", err)
	}

	return nil
}

func ShellContainer(host string) error {
	name := containerName(host)
	shell := config.Shell(host)

	err := PodmanShell(
		"exec",
		"--interactive",
		"--tty",
		name,
		shell,
	)
	if err != nil {
		return fmt.Errorf("Unable to shell into podman container: %v", err)
	}

	return nil
}

func StartContainer(host string) error {
	name := containerName(host)

	err := PodmanRun("start", name)
	if err != nil {
		return fmt.Errorf("Unable to start podman container: %v", err)
	}

	return nil
}

func StopContainer(host string) error {
	name := containerName(host)

	err := PodmanRun("stop", name)
	if err != nil {
		return fmt.Errorf("Unable to top podman container: %v", err)
	}

	return nil
}
