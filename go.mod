module jerrykan.com/podgrim

go 1.15

require github.com/spf13/cobra v1.2.1

require (
	github.com/containers/podman/v3 v3.0.0-20210301222049-8af66806c804 // indirect
	github.com/google/uuid v1.2.0
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/spf13/viper v1.8.1
)
